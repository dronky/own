package com.sinky.services.own;

import com.sinky.services.own.config.OtherProperties;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@EnableConfigurationProperties(OtherProperties.class)
@RunWith(SpringRunner.class)
@WebMvcTest(HelloController.class)
public class HelloControllerTests {

    @Autowired
    OtherProperties otherProperties;

    @Autowired
    private MockMvc mvc;

    @Test
    public void givenGoodUrl_thenReturnGreetings()
            throws Exception {

        otherProperties.setUrl("https://gturnquist-quoters.cfapps.io/api/random");
        otherProperties.setOtherTimeout(1000);

        mvc.perform(post("/hello")
                .param("name", "Andrey")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.reply").value("Andrey, сервис приветствует тебя"));
    }

    @Test
    public void givenBadUrl_thenReturnNotFound()
            throws Exception {

        otherProperties.setUrl("https://gturnquist-quoters.cfapps.io/api/random/wrong-path");
        otherProperties.setOtherTimeout(1000);


        mvc.perform(post("/hello")
                .param("name", "Andrey")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.reply").value("Извините, Andrey, но я вас не знаю."));
    }

    @Test
    public void givenUnresponsiveUrl_thenReturnNoResponse()
            throws Exception {

        otherProperties.setUrl("http://example.com:81");
        otherProperties.setOtherTimeout(1000);

        mvc.perform(post("/hello")
                .param("name", "Andrey")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.reply").value("Никого нет дома"));
    }

    @Test
    public void givenSmallTimeout_thenReturnNoResponse()
            throws Exception {

        otherProperties.setUrl("https://gturnquist-quoters.cfapps.io/api/random");
        otherProperties.setOtherTimeout(1);

        mvc.perform(post("/hello")
                .param("name", "Andrey")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.reply").value("Никого нет дома"));
    }


}