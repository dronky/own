package com.sinky.services.own;

public class Hello {

    private String reply;

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

    public Hello(String name, int statusCode) {
        switch (statusCode) {
            case 200:
                this.reply = String.format("%s, сервис приветствует тебя", name);
                break;
            case 404:
                this.reply = String.format("Извините, %s, но я вас не знаю.", name);
                break;
            case -1:
                this.reply = "Никого нет дома";
                break;
            default:
                this.reply = "Произошла ошибка";
        }
    }
}
