package com.sinky.services.own.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:other.properties")
@ConfigurationProperties(prefix = "other")
public class OtherProperties {

    private String url;

    private int otherTimeout;

    private HttpComponentsClientHttpRequestFactory clientHttpRequestFactory;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getOtherTimeout() {
        return otherTimeout;
    }

    public void setOtherTimeout(int otherTimeout) {
        this.otherTimeout = otherTimeout;
    }


    public void setClientHttpRequestFactory(HttpComponentsClientHttpRequestFactory clientHttpRequestFactory) {
        this.clientHttpRequestFactory = clientHttpRequestFactory;
    }

    public ClientHttpRequestFactory getClientHttpRequestFactory() {
        this.clientHttpRequestFactory
                = new HttpComponentsClientHttpRequestFactory();
        clientHttpRequestFactory.setConnectTimeout(otherTimeout);
        clientHttpRequestFactory.setConnectionRequestTimeout(otherTimeout);
        clientHttpRequestFactory.setReadTimeout(otherTimeout);
        return clientHttpRequestFactory;
    }

}
