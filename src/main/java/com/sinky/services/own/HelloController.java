package com.sinky.services.own;

import com.sinky.services.own.config.OtherProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@RestController
public class HelloController {

    @Autowired
    OtherProperties otherProperties;

    @RequestMapping(path = "/hello", method = RequestMethod.POST)
    public Hello hello(@RequestParam(value = "name") String name) {
        return new Hello(name, getOther(name, otherProperties.getUrl(), otherProperties.getClientHttpRequestFactory()));
    }

    //  http://example.com:81 - return timeout
    //  https://gturnquist-quoters.cfapps.io/api/random - return 200
    //  https://gturnquist-quoters.cfapps.io/api/random/aaa - return 404

    //Returns status code, -1 when timeout occurs, -2 when unhandled error occurs
    public int getOther(String name, String url, ClientHttpRequestFactory clientHttpRequestFactory) {

        // get getClientHttpRequestFactory with timeout configured
        RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory);

        try {
            ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
//            ResponseEntity<String> response = restTemplate.getForEntity(url + "/greeting/" + name, String.class);
            return response.getStatusCode().value();
        } catch (RestClientException e) {

            // Status code
            if (e instanceof HttpClientErrorException) {
                HttpClientErrorException clientEx = ((HttpClientErrorException) e);
                return clientEx.getStatusCode().equals(HttpStatus.NOT_FOUND) ? HttpStatus.NOT_FOUND.value() : clientEx.getStatusCode().value();
            }
            // Timeout
            else if (e instanceof ResourceAccessException) {
                return -1;
            } else System.out.println("Unhandled RestClientException:" + e);

        }

        return -2;
    }

//    }    private int getOther(String name) {
//
//        int status = -1;
//        try {
//            URL url = new URL("https://gturnquist-quoters.cfapps.io/api/random");
//            HttpURLConnection con = (HttpURLConnection) url.openConnection();
//            con.setRequestMethod("GET");
//            con.setConnectTimeout(1000);
//            con.setReadTimeout(1000);
//            status = con.getResponseCode();
//            System.out.println("GET STATUS CODE:" + status);
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        } catch (ProtocolException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//return status;
//    }

}