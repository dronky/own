# Demo service - "own

This is demo service, which do following:

* handle POST request to /hello
* call another service, which can return:
1. 200 code
2. 404 code
3. timeout

## Getting Started

* configure url and timeout in property file: "other.properties"
* you should have maven installed
## Running the tests

```
mvn test
mvn -Dtest=HelloControllerTests test
```

## Build

```
mvn clean install
```

## Run with specific url and timeout value

```
edit properties file :)
```

## Authors

* **Andrey Makarenko**  - [RussianRailways](gvc.rzd.ru)